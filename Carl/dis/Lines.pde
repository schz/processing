class Lines{
  
  private float y, len;
  private boolean alive;
  private boolean blink;
  private float velocity;
  private float alpha;
  
  Lines(float x, float y, int len, float velocity){
    this.y = y;
    this.len = len;
    this.alive = true;
    this.blink = false;
    this.velocity = velocity;
    this.alpha = 0;
  }
  
  Lines(float x, float y, int len, float velocity, boolean blink){
    this.y = y;
    this.len = len;
    this.alive = true;
    this.blink = false;
    this.velocity = velocity;
    this.alpha = 0;
    this.blink = blink;
  }    

  public void setBlink(boolean blink){
    this.blink = blink;  
  }
  
  public boolean getBlink(){
      return this.blink;
  }

  public float getY(){
    return this.y;
  }
  
  public float[] getPosition(){
    return new float[] {this.y};
  }

  public void setAlive(boolean alive){
    this.alive = alive;
  }

  public boolean getAlive(){
    return this.alive;
  }
  
  public void update(float y){
        this.y = y;
      
  }
  
  public float  getVelocity(){
    return this.velocity;
  }
    
  public void draw(){    
  if(this.alive){   
    if(blink){
      stroke(0, 0, 0, blink(this.alpha)); 
      line(0, this.y, len, this.y);  
    }
    else{

    }    
    println("alpha: " + this.alpha + " velocity: " + this.velocity);
  }
  
  private float blink(float alpha){
    this.alpha += this.velocity;
    if(this.alpha > 255 || this.alpha < 0){    
      this.velocity = -this.velocity;       
    }
  return velocity;
  }  
}