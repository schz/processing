class Lines{
  
  private float y, len;
  private boolean alive;
  private boolean blink;
  
  Lines(float x, float y, int len, float velocity){
    this.y = y;
    this.len = len;
    this.alive = false;
    this.blink = blink;
  }
  
  public void setBlink(boolean blink){
    this.blink = blink;  
  }
  
  public boolean getBlink(){
      return this.getBlink;
  }

  public float getY(){
    return this.y;
  }
  
  public float[] getPosition(){
    return new float[] {this.y};
  }

  public void setAlive(boolean alive){
    this.alive = alive;
  }

  public boolean getAlive(){
    return this.alive;
  }
  
  public void update(float y){
        this.y = y;
      
  }
  
    public void getVelocity(){
      return this.velocity;
  }
    
    public void draw(){
    
    if(this.alive){   
        if(blink){
         stroke(0, 0, 0, blink(alpha);   
        }
        line(0, this.y, len, this.y);
    }
    
  }
  
    private float blink(float alpha){
        if(alpha > 255 || alpha < 0){
            v = -v; 
    }
    return v;
  }
  
}