import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class lines extends PApplet {

int size = 30;
Line[] reta = new Line[size];
int state = 1;

public void setup(){
 size(400, 300); 
 
 for(int i = 0; i < reta.length; i++){
  reta[i] = new Line(0, 0, 400);
  } 
}

public void update(){  
  for(int i = 0; i < reta.length; i++){
  reta[i].update(random(400));
  }   
}

public void draw(){
 update();
 background(255, 255, 255);
 
  for(int i = 0; i < size; i++){
  reta[i].draw();
  } 
}
public void mouseDragged(){
  println(size);
  if(pmouseY < mouseY){
    size--; 
    println(size);
 
    if(size < 0){
      size = 0; 
 
    }   
  }
  else{
   size++;
    
    if(size >30){
      size = 30;  
    } 
 
 }
}
class Line{
  
  private float y, len;
  
  Line(float x, float y, int len){
    this.y = y;
    this.len = len;
  }

  public float getY(){
    return this.y;
  }
  
  public float[] getPosition(){
    return new float[] {this.y};
  }
  
  public void update(float y){
      this.y = y;
  }
   
  public void draw(){
    line(0, this.y, len, this.y);
    
  }
  
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "lines" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
