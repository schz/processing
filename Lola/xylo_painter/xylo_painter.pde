////Xylo_painter - Heloisa Souza, 2014.

Maxim maxim;
AudioPlayer[] player = new AudioPlayer[8];
String arquivo;

void setup()
{
  size(500,500);
  background(255);
  rectMode(CENTER);

  maxim = new Maxim(this);
for (int i = 0; i < player.length; i++)
  {
    //arquivo = str(i) + ".wav";
    player[i] = maxim.loadFile(str(i + 1) + ".wav");
    player[i].setLooping(false);
    player[i].setFilter((float) mouseY/height*5000,mouseX / width);
    player[i].ramp(1.,1000);
  }
}

void draw()
{
//
}

void mouseDragged()
{
  //line(pmouseX, pmouseY, mouseX, mouseY);
  float red = map(mouseX, 0, width, 0, 255);
  float blue = map(mouseY, 0, width, 0, 255);
  float green = dist(mouseX,mouseY,width/2,height/2);
  
  float speed = dist(pmouseX, pmouseY, mouseX, mouseY);
  float alpha = map(speed, 0, 20, 0, 10);
  //println(alpha);
  float lineWidth = map(speed, 0, 10, 10, 1);
  lineWidth = constrain(lineWidth, 0, 10);
  
  noStroke();
  fill(0, alpha);
  rect(width/2, height/2, width, height);
  
  stroke(red, green, blue, 255);
  strokeWeight(lineWidth);
  
  //rect(mouseX, mouseY, speed, speed);
  line(pmouseX, pmouseY,mouseX, mouseY);
  
  
  if (mouseX>0 && mouseX<250 && mouseY>0 && mouseY<250 && mouseX<mouseY)
  {
    player[0].play();
    println("1 - C");
  }  

  if (mouseX>0 && mouseX<250 && mouseY>0 && mouseY<250 && mouseX>mouseY)
  {
    player[1].play();
    println("2 - D");
  }  
  
    if (mouseX>250 && mouseX<500 && mouseY>0 && mouseY<250 && mouseX-250<mouseY)
  {
    player[2].play();
    println("3 - E");
  }  
  if (mouseX>250 && mouseX<500 && mouseY>0 && mouseY<250 && mouseX-250>mouseY)
  {
    player[3].play();
    println("4 - F");
  }  

  if (mouseX>250 && mouseX<500 && mouseY>250 && mouseY<500 && mouseX>mouseY)
  {
    player[4].play();
    println("5 - G");
  }  

  if (mouseX>250 && mouseX<500 && mouseY>250 && mouseY<500 && mouseX<mouseY)
  {
    player[5].play();
    println("6 - A");
  }  

  if (mouseX>0 && mouseX<250 && mouseY>250 && mouseY<500 && mouseX>mouseY-250)
  {
    player[6].play();
    println("7 - B");
  }  

  if (mouseX>0 && mouseX<250 && mouseY>250 && mouseY<500 && mouseX<mouseY-250)
  {
    player[7].play();
    println("8 - C2");
  }  

}
