import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Paint extends PApplet {

public void setup()
{
  size(640, 480);
  background(255);
}

public void draw()
{ 
  if (mousePressed) 
  {
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
  rect(10, 100, 10, 10);
  fill(255, 0, 0);
  rect(10, 120, 10, 10); 
  fill(0, 255, 0);
  rect(10, 140, 10, 10);
  fill(0, 179, 255);
  rect(10, 160, 10, 10);
  fill(255, 251, 0);
  rect(10, 180, 10, 10);
  fill(0);
  rect(10, 200, 10, 10);
  fill(255);
}

//void mouseClicked()
//{
 // if (mouseX>10 && mouseX<20 && mouseY>100 && mouseY<110)
  //{
  //if (mousePressed) 
 //{
 //line (pmouseX, pmouseY, mouseX, mouseY);
 //stroke(255, 0, 0);
  //println("Linha agora \u00e9 vermelho.");
 //}
  //}
//}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Paint" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
