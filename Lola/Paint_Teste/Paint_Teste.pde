void setup()
{
  size(900, 600);
  background(255);

void draw()
{
  fill(255, 0, 0);
  rect(100, 100, 50, 50);
  fill(0, 0, 255);
  rect(100, 200, 50, 50);
  fill(0, 255, 0);
  rect(100, 300, 50, 50);

  if (mouseX > 100 && mouseX < 150 && mouseY>100 && mouseY<150)
  {
    if (mousePressed)
    {
      println("red");
      stroke(255, 0, 0);
    }
  }

  if (mouseX>100 && mouseX<150 && mouseY>200 && mouseY<250)
  {
    if (mousePressed)
    {
      println("blue");
      stroke(0, 0, 255);
    }
  }

  if (mouseX>100 && mouseX<150 && mouseY>300 && mouseY<350)
  {
    if (mousePressed) 
    {
      stroke(0, 255, 0);
      println("green");
    }
    
  }

  if (mousePressed)
  {
    line (pmouseX, pmouseY, mouseX, mouseY);
    
  }

}

