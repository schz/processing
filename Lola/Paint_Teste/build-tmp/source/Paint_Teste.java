import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Paint_Teste extends PApplet {

public void setup()
{
  size(900, 600);
  background(255);
}

public void draw()
{
  fill(255, 0, 0);
  rect(100, 100, 50, 50);
  fill(0, 0, 255);
  rect(100, 200, 50, 50);
  fill(0, 255, 0);
  rect(100, 300, 50, 50);

  if (mouseX > 100 && mouseX < 150 && mouseY>100 && mouseY<150)
  {
    if (mousePressed)
    {
      println("red");
      stroke(255, 0, 0);
    }
  }

  if (mouseX>100 && mouseX<150 && mouseY>200 && mouseY<250)
  {
    if (mousePressed)
    {
      println("blue");
      stroke(0, 0, 255);
    }
  }

  if (mouseX>100 && mouseX<150 && mouseY>300 && mouseY<350)
  {
    if (mousePressed) 
    {
      stroke(0, 255, 0);
      println("green");
    }
    
  }

  if (mousePressed)
  {
    line (pmouseX, pmouseY, mouseX, mouseY);
    
  }

}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Paint_Teste" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
