import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class rotating extends PApplet {

float r = 0;

public void setup(){
	size(400, 400);
	background(255);
	noStroke();
	fill(0);
	rectMode(CENTER);


}

public void draw(){
	background(255);
	
	translate(100, 100);
	rotate(r);
	rect(0, 0, 80, 80);
	resetMatrix();

	translate(300, 100);
	rotate(r);
	rect(0, 0, 80, 80);
	resetMatrix();
	
	translate(100, 300);
	rotate(r);
	rect(0, 0, 80, 80);
	resetMatrix();
	
	translate(300, 300);
	rotate(r);
	rect(0, 0, 80, 80);
	resetMatrix();


	r = r + 0.2f;
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "rotating" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
