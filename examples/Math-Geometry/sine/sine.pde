size(300, 300);
background(#357BC4);

stroke(#D66727);
line(0, 50, width, 50);
line(0, 150, width, 150);
line(0, 250, width, 250);

stroke(255);

float x = 0;

while(x < width){
	point(x, 50 + random(-10, 10));
	point(x, 150 + 20 * noise(x/10));
	point(x, 250 + 20 * sin(x/10));

	x = x + 1;
}