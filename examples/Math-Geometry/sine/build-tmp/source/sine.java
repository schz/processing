import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class sine extends PApplet {
  public void setup() {
size(300, 300);
background(0xff357BC4);

stroke(0xffD66727);
line(0, 50, width, 50);
line(0, 150, width, 150);
line(0, 250, width, 250);

stroke(255);

float x = 0;

while(x < width){
	point(x, 50 + random(-10, 10));
	point(x, 150 + 20 * noise(x/10));
	point(x, 250 + 20 * sin(x/10));

	x = x + 1;
}
    noLoop();
  }

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "sine" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
