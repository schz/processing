float my_num = 10;

void setup(){
	size(400, 400);
	fill(255);
	noStroke();
	smooth();
	background(0);
	rectMode(CENTER);
}

void draw(){
	background(0);

	translate(100, 100);
	rotate(10 * noise(my_num));
	rect(0, 0, 200 * noise(my_num), noise(my_num) + 20);
	my_num += 1;

}