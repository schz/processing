import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class noise extends PApplet {

float my_num = 10;

public void setup(){
	size(400, 400);
	fill(255);
	noStroke();
	smooth();
	background(0);
	rectMode(CENTER);
}

public void draw(){
	background(0);

	translate(100, 100);
	rotate(10 * noise(my_num));
	rect(0, 0, 200 * noise(my_num), noise(my_num) + 20);
	my_num += 1;

}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "noise" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
