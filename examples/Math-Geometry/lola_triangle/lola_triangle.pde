Maxim maxim;
AudioPlayer[] player = new AudioPlayer[8];
Triangle[] t = new Triangle[8];

void setup() {
  maxim = new Maxim(this);
  size(500, 500);

  t[0]= new Triangle(0, 0, 250, 0, 250, 250);
  t[1] = new Triangle(250, 0, 500, 0, 250, 250);
  t[2] = new Triangle(500, 250, 500, 0, 250, 250);
  t[3] = new Triangle(500, 500, 250, 250, 500, 250);
  t[4] = new Triangle(250, 500, 500, 500, 250, 250);
  t[5] = new Triangle(0, 500, 250, 500, 250, 250);
  t[6] = new Triangle(0, 500, 0, 250, 250, 250);
  t[7] = new Triangle(0, 250, 0, 0, 250, 250);
  
  for(int i = 0; i < player.length; i++){
    player[i] = maxim.loadFile(str(i + 1)  + ".wav");
    player[i].setLooping(false);
  
}


void draw(){
  background(255);

  for(int i = 0; i < t.length; i++){
    t[i].drawTriangle();
    //player[i].play();
  


  for(int i = 0; i < t.length; i++){    

    if(t[i].checkCollision(mouseX, mouseY))
    {    
      player[i].play();
      t[i].setTriangleColor(random(255), random(255), random(255));
    
}
    else
    {
      
   
}
  
}