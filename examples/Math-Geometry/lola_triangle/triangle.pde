class Triangle {
  float point1x;
  float point1y;
  float point2x;
  float point2y;
  float point3x;
  float point3y;
  float r, g, b, alpha;
  
  Triangle(float point1x,float point1y,float point2x,float point2y,float point3x,float point3y, float r, float g, float b)
  {
    this.point1x = point1x;
    this.point1y = point1y;
    this.point2x = point2x;
    this.point2y = point2y;
    this.point3x = point3x;
    this.point3y = point3y;       
    this.r = r;
    this.g = g;
    this.b = b;
  }

  Triangle(float point1x,float point1y,float point2x,float point2y,float point3x,float point3y)
  {
    this.point1x = point1x;
    this.point1y = point1y;
    this.point2x = point2x;
    this.point2y = point2y;
    this.point3x = point3x;
    this.point3y = point3y;       
    this.r = r;
    this.g = g;
    this.b = b;
    this.alpha = 0;
  
  }

  void setTriangleColor(float r, float g, float b)
  {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  boolean checkCollision(float x, float y) 
  {
    float tArea,t1Area,t2Area,t3Area;
    tArea  = triangleArea(this.point1x, this.point1y, this.point3x, this.point3y, this.point2x, this.point2y);
    t1Area = triangleArea(x,y, this.point2x, this.point2y, this.point3x, this.point3y);
    t2Area = triangleArea(x,y, this.point3x, this.point3y, this.point1x, this.point1y);
    t3Area = triangleArea(x,y, this.point2x, this.point2y, this.point1x, this.point1y);
 
    float totalArea = t1Area+t2Area+t3Area;
    return(totalArea == tArea);
  }

  float triangleArea(float p1, float p2, float p3, float p4, float p5, float p6) 
  {
    float a,b,c,d;
    a = p1 - p5;
    b = p2 - p6;
    c = p3 - p5;
    d = p4 - p6;
    return (0.5* abs((a*d)-(b*c)));
  }

  void drawTriangle() 
  {
    fill(this.r, this.g, this.b);
    triangle(point1x, point1y, point2x, point2y, point3x, point3y);
  }
}