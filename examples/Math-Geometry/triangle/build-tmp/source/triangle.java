import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class triangle extends PApplet {

Triangle t;
 
public void setup() {
  size(300,300);
  t = new Triangle(width/2,height/4,width-width/4,height-height/4,width/4,height-height/4);
}
 
public void draw() {
  background(0);
  t.drawTriangle();
  if(checkCollision(mouseX,mouseY,t)){
    fill(0);
    stroke(0xff00ff00);
  }
  else{
    noStroke();
  }
}
 
 
 
class Triangle {
  float point1x;
  float point1y;
  float point2x;
  float point2y;
  float point3x;
  float point3y;
   
  Triangle(float point1x,float point1y,float point2x,float point2y,float point3x,float point3y){
  this.point1x = point1x;
  this.point1y = point1y;
  this.point2x = point2x;
  this.point2y = point2y;
  this.point3x = point3x;
  this.point3y = point3y;       
  }
   
  public void drawTriangle() {
    triangle(point1x, point1y, point2x, point2y, point3x, point3y);
  }
}
 
public boolean checkCollision(float x, float y, Triangle t) {
  float tArea,t1Area,t2Area,t3Area;
  tArea  = triangleArea(t.point1x, t.point1y, t.point3x, t.point3y, t.point2x, t.point2y);
  t1Area = triangleArea(x,y, t.point2x, t.point2y, t.point3x, t.point3y);
  t2Area = triangleArea(x,y, t.point3x, t.point3y, t.point1x, t.point1y);
  t3Area = triangleArea(x,y, t.point2x, t.point2y, t.point1x, t.point1y);
  noStroke();
  fill(0xffff6600,50);
  triangle(t.point1x, t.point1y, t.point3x, t.point3y, t.point2x, t.point2y);
  fill(0xffffff00,50);
  triangle(x,y, t.point2x, t.point2y, t.point3x, t.point3y);
  fill(0xff00ffff,50);
  triangle(x,y, t.point3x, t.point3y, t.point1x, t.point1y);
  fill(0xffff0000,50);
  triangle(x,y, t.point2x, t.point2y, t.point1x, t.point1y);
   
  float totalArea = t1Area+t2Area+t3Area;
  return (totalArea == tArea);
}
 
public float triangleArea(float p1, float p2, float p3, float p4, float p5, float p6) {
  float a,b,c,d;
  a = p1 - p5;
  b = p2 - p6;
  c = p3 - p5;
  d = p4 - p6;
  return (0.5f* abs((a*d)-(b*c)));
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "triangle" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
