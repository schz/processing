import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class lines extends PApplet {

class Lines{

  private float y, len;
  private boolean alive;
  private boolean blink;
  private float velocity;

  Lines(float x, float y, int len, float velocity){
  this.y = y;
  this.len = len;
  this.alive = true;
  this.blink = false;
  this.velocity = velocity;
  }

  public void setBlink(boolean blink){
  this.blink = blink;  
  }

  public boolean getBlink(){
    return this.blink;
  }

  public float getY(){
  return this.y;
  }

  public float[] getPosition(){
  return new float[] {this.y};
  }

  public void setAlive(boolean alive){
  this.alive = alive;
  }

  public boolean getAlive(){
  return this.alive;
  }

  public void update(float y){
  this.y = y;
  }

  public float getVelocity(){
  return this.velocity;
  }

  public void draw(){

    if(this.alive){   
        if(blink){
        stroke(0, 0, 0, alpha);   
        }
      line(0, this.y, len, this.y);
    }
  }

  private void blink(){
    if(alpha > 255 || alpha < 0){
    v = -v; 
    } 
  }
}


public void setup(){
}

public void draw(){
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "lines" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
