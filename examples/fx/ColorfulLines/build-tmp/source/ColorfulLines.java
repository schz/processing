import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ColorfulLines extends PApplet {

public void draw(){
	stroke(random(256));
	float distance_left = random(100);
	line(distance_left, 0, distance_left, 99);
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "ColorfulLines" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
