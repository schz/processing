import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class letras extends PApplet {

//http://www.processing.org/tutorials/text/

PFont f;                          // STEP 2 Declare PFont variable
  
public void setup() {
  size(200,200);
  f = createFont("Arial",16,true); // STEP 3 Create Font
}

public void draw() {
  background(255);
  textFont(f,16);                 // STEP 4 Specify font to be used
  fill(0);                        // STEP 5 Specify font color 
  text("Hello Strings!",10,200);  // STEP 6 Display Text
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "letras" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
