import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class audio extends PApplet {

Maxim maxim;
AudioPlayer player;

public void setup(){
	maxim = new Maxim(this);
	player = maxim.loadFile("Tech-59.wav");

}

public void draw(){
	
}

public void mousePressed(){
	player.play();
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "audio" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
