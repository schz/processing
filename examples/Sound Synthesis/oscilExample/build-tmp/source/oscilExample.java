import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import ddf.minim.*; 
import ddf.minim.ugens.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class oscilExample extends PApplet {

/* oscilExample<br/>
   is an example of using the Oscil UGen inside an instrument.
   <p>
   For more information about Minim and additional features, visit http://code.compartmental.net/minim/
   <p>
   author: Anderson Mills<br/>
   Anderson Mills's work was supported by numediart (www.numediart.org)
*/

// import everything necessary to make sound.



// create all of the variables that will need to be accessed in
// more than one methods (setup(), draw(), stop()).
Minim minim;
AudioOutput out;

// setup is run once at the beginning
public void setup()
{
  // initialize the drawing window
  size( 512, 200, P2D );

  // initialize the minim and out objects
  minim = new Minim( this );
  out = minim.getLineOut( Minim.MONO, 2048 );
  
  // initialize the myNote object as a ToneInstrument
  ToneInstrument myNote = new ToneInstrument( 587.3f, 0.9f, out );
  // play a note with the myNote object
  out.playNote( 0.5f, 2.6f, myNote );
  // give a new note value to myNote
  myNote = new ToneInstrument( 415.3f, 0.9f, out );
  // play another note with the myNote object
  out.playNote(3.5f, 2.6f, myNote );
}

// draw is run many times
public void draw()
{
  // erase the window to black
  background( 0 );
  // draw using a white stroke
  stroke( 255 );
  // draw the waveforms
  for( int i = 0; i < out.bufferSize() - 1; i++ )
  {
    // find the x position of each buffer value
    float x1  =  map( i, 0, out.bufferSize(), 0, width );
    float x2  =  map( i+1, 0, out.bufferSize(), 0, width );
    // draw a line from one buffer position to the next for both channels
    line( x1, 50 - out.left.get(i)*50, x2, 50 - out.left.get(i+1)*50);
    line( x1, 150 - out.right.get(i)*50, x2, 150 - out.right.get(i+1)*50);
  }  
}
// Every instrument must implement the Instrument interface so 
// playNote() can call the instrument's methods.
class ToneInstrument implements Instrument
{
  // create all variables that must be used throughout the class
  Oscil sineOsc, lFOOsc;
  Multiplier  multiplyGate;
  AudioOutput out;
  
  // constructors for this intsrument
  ToneInstrument( float frequency, float amplitude, AudioOutput output )
  {
    // equate class variables to constructor variables as necessary 
    out = output;
    
    // create new instances of any UGen objects as necessary
    sineOsc = new Oscil( frequency, amplitude, Waves.SINE );
    multiplyGate = new Multiplier( 0 );
    
    // patch everything together up to the final output
    sineOsc.patch( multiplyGate );
  }
  
  // every instrument must have a noteOn( float ) method
  public void noteOn( float dur )
  {
    // turn on the multiply
    multiplyGate.setValue( 1.0f );
    // and patch to the output
    multiplyGate.patch( out );
  }
  
  // every instrument must have a noteOff() method
  public void noteOff()
  {
    // turn off the multiply
    multiplyGate.setValue( 0 );
    // and unpatch the output 
    // this causes the entire instrument to stop calculating sampleframes
    // which is good when the instrument is no longer generating sound.
    multiplyGate.unpatch( out );
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "oscilExample" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
